# translation of ksmserver.po to Persian
# Nazanin Kazemi <kazemi@itland.ir>, 2006, 2007.
# MaryamSadat Razavi <razavi@itland.ir>, 2006.
# Tahereh Dadkhahfar <dadkhahfar@itland.ir>, 2006.
# Nasim Daniarzadeh <daniarzadeh@itland.ir>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: ksmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-27 02:13+0000\n"
"PO-Revision-Date: 2007-06-14 11:12+0330\n"
"Last-Translator: Nazanin Kazemi <kazemi@itland.ir>\n"
"Language-Team: Persian <kde-i18n-fa@kde.org>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: logout.cpp:340
#, kde-format
msgid "Logout canceled by '%1'"
msgstr "خروج توسط »%1« لغو شد"

#: main.cpp:74
#, kde-format
msgid "$HOME not set!"
msgstr ""

#: main.cpp:78 main.cpp:86
#, kde-format
msgid "$HOME directory (%1) does not exist."
msgstr ""

#: main.cpp:81
#, kde-kuit-format
msgctxt "@info"
msgid ""
"No write access to $HOME directory (%1). If this is intentional, set "
"<envar>KDE_HOME_READONLY=1</envar> in your environment."
msgstr ""

#: main.cpp:88
#, kde-format
msgid "No read access to $HOME directory (%1)."
msgstr ""

#: main.cpp:92
#, kde-format
msgid "$HOME directory (%1) is out of disk space."
msgstr ""

#: main.cpp:95
#, kde-format
msgid "Writing to the $HOME directory (%2) failed with the error '%1'"
msgstr ""

#: main.cpp:108 main.cpp:143
#, kde-format
msgid "No write access to '%1'."
msgstr ""

#: main.cpp:110 main.cpp:145
#, kde-format
msgid "No read access to '%1'."
msgstr ""

#: main.cpp:118 main.cpp:131
#, kde-format
msgid "Temp directory (%1) is out of disk space."
msgstr ""

#: main.cpp:121 main.cpp:134
#, kde-format
msgid ""
"Writing to the temp directory (%2) failed with\n"
"    the error '%1'"
msgstr ""

#: main.cpp:149
#, kde-format
msgid ""
"The following installation problem was detected\n"
"while trying to start Plasma:"
msgstr ""

#: main.cpp:152
#, kde-format
msgid ""
"\n"
"\n"
"Plasma is unable to start.\n"
msgstr ""

#: main.cpp:159
#, kde-format
msgid "Plasma Workspace installation problem!"
msgstr ""

#: main.cpp:193
#, fuzzy, kde-format
#| msgid ""
#| "The reliable KDE session manager that talks the standard X11R6 \n"
#| "session management protocol (XSMP)."
msgid ""
"The reliable Plasma session manager that talks the standard X11R6 \n"
"session management protocol (XSMP)."
msgstr ""
"مدیریت نشست مطمئن KDE که استاندارد X11R6 را می‌گوید\n"
"قرارداد مدیریت نشست )XSMP(."

#: main.cpp:197
#, kde-format
msgid "Restores the saved user session if available"
msgstr "نشست ذخیره‌شده کاربر را در صورت امکان باز می‌گرداند"

#: main.cpp:200
#, kde-format
msgid "Also allow remote connections"
msgstr "همچنین اجازه اتصالات دور را می‌دهد"

#: main.cpp:203
#, kde-format
msgid "Starts the session in locked mode"
msgstr ""

#: main.cpp:207
#, kde-format
msgid ""
"Starts without lock screen support. Only needed if other component provides "
"the lock screen."
msgstr ""

#: server.cpp:881
#, fuzzy, kde-format
#| msgid "The KDE Session Manager"
msgid "Session Management"
msgstr "مدیر نشست KDE"

#: server.cpp:886
#, fuzzy, kde-format
#| msgid "Logout"
msgid "Log Out"
msgstr "خروج"

#: server.cpp:891
#, kde-format
msgid "Shut Down"
msgstr ""

#: server.cpp:896
#, kde-format
msgid "Reboot"
msgstr ""

#: server.cpp:902
#, kde-format
msgid "Log Out Without Confirmation"
msgstr ""

#: server.cpp:907
#, kde-format
msgid "Shut Down Without Confirmation"
msgstr ""

#: server.cpp:912
#, kde-format
msgid "Reboot Without Confirmation"
msgstr ""

#, fuzzy
#~| msgid ""
#~| "Starts 'wm' in case no other window manager is \n"
#~| "participating in the session. Default is 'kwin'"
#~ msgid ""
#~ "Starts <wm> in case no other window manager is \n"
#~ "participating in the session. Default is 'kwin'"
#~ msgstr ""
#~ "چنانچه مدیر پنجره دیگری در نشست شرکت نداشته باشد، »wm« را آغاز می‌کند.\n"
#~ "پیش‌فرض »kwin« می‌باشد"

#, fuzzy
#~| msgid "Logout"
#~ msgid "Logout"
#~ msgstr "خروج"

#, fuzzy
#~ msgid "Sleeping in 1 second"
#~ msgid_plural "Sleeping in %1 seconds"
#~ msgstr[0] "خاموش کردن رایانه"

#, fuzzy
#~ msgid "Logging out in 1 second."
#~ msgid_plural "Logging out in %1 seconds."
#~ msgstr[0] "خاموش کردن رایانه"

#, fuzzy
#~ msgid "Turning off computer in 1 second."
#~ msgid_plural "Turning off computer in %1 seconds."
#~ msgstr[0] "خاموش کردن رایانه"

#, fuzzy
#~ msgid "Restarting computer in 1 second."
#~ msgid_plural "Restarting computer in %1 seconds."
#~ msgstr[0] "خاموش کردن رایانه"

#, fuzzy
#~| msgid "Turn Off Computer"
#~ msgid "Turn Off Computer"
#~ msgstr "خاموش کردن رایانه"

#, fuzzy
#~| msgid "Restart Computer"
#~ msgid "Restart Computer"
#~ msgstr "بازآغازی رایانه"

#, fuzzy
#~| msgid "Cancel"
#~ msgid "Cancel"
#~ msgstr "لغو"

#, fuzzy
#~| msgid "Standby"
#~ msgid "&Standby"
#~ msgstr "نیمه‌روشن"

#, fuzzy
#~| msgid "Suspend to RAM"
#~ msgid "Suspend to &RAM"
#~ msgstr "معلق در RAM"

#, fuzzy
#~| msgid "Suspend to Disk"
#~ msgid "Suspend to &Disk"
#~ msgstr "معلق در دیسک"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "نازنین کاظمی"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "kazemi@itland.ir"

#~ msgid "(C) 2000, The KDE Developers"
#~ msgstr "(ح) ۲۰۰۰، توسعه‌دهندگان KDE"

#~ msgid "Matthias Ettrich"
#~ msgstr "Matthias Ettrich"

#~ msgid "Luboš Luňák"
#~ msgstr "Luboš Luňák"

#~ msgid "Maintainer"
#~ msgstr "نگه‌دارنده"

#~ msgctxt "current option in boot loader"
#~ msgid " (current)"
#~ msgstr " )جاری("

#~ msgctxt "@label In corner of the logout dialog"
#~ msgid "KDE <numid>%1.%2.%3</numid>"
#~ msgstr "KDE <numid>%1.%2.%3</numid>"

#~ msgctxt "@label In corner of the logout dialog"
#~ msgid "KDE <numid>%1.%2</numid>"
#~ msgstr "KDE <numid>%1.%2</numid>"

#~ msgid "End Session for %1"
#~ msgstr "پایان نشست برای %1"

#~ msgid "End Session for %1 (%2)"
#~ msgstr "پایان نشست برای %1 (%2)"
